# symfony/dependency-injection

Allows to standardize and centralize the way objects are constructed https://packagist.org/packages/symfony/dependency-injection

[![PHPPackages Rank](http://phppackages.org/p/symfony/dependency-injection/badge/rank.svg)](http://phppackages.org/p/symfony/dependency-injection)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/dependency-injection/badge/referenced-by.svg)](http://phppackages.org/p/symfony/dependency-injection)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation: inspiration
* https://symfony.com/dependency-injection
* From Java Spring framework:
  * [*Open-Source cross-pollination*
  ](https://symfony.com/blog/open-source-cross-pollination)
  2008-09 Fabien Potencier

# Official documentation
* [*New in Symfony 5.1: Stack decorators*
  ](https://symfony.com/blog/new-in-symfony-5-1-stack-decorators)
  2020-04 Javier Eguiluz, Nicolas Grekas

# Unofficial documentation
* [*Improve your factories in Symfony*
  ](https://dev.to/mguinea/improve-your-factories-in-symfony-4mf3)
  2022-03 Marc Guinea
* [*4 ways to inject dependencies in Symfony that you probably don’t know about?*
  ](https://medium.com/@OCharnyshevich/4-ways-to-inject-dependencies-in-symfony-that-you-probably-dont-know-about-65e8efe02af5)
  2022-01 Oleg Charnyshevich
* [*How to retrieve specific and all YAML parameters from services.yaml in Symfony 4*
  ](https://ourcodeworld.com/articles/read/1041/how-to-retrieve-specific-and-all-yaml-parameters-from-services-yaml-in-symfony-4)
  2019-09 Carlos Delgado
* [*Why Config Coding Sucks*
  ](https://www.tomasvotruba.cz/blog/2019/02/14/why-config-coding-sucks/)
  2019-02 Tomas Votruba
* [*Diving into Symfony’s DependencyInjection — Part 1: First steps with the container*](https://medium.com/manomano-tech/diving-into-symfonys-dependencyinjection-part-1-first-steps-with-the-container-2fad0593c052)
  2018 [Alex Makdessi](https://medium.com/@alex.makdessi)
* [*Understanding Dependency Injection by example with the Symfony DI component (part 1/2)*](https://medium.com/@galopintitouan/understanding-dependency-injection-by-example-with-the-symfony-di-component-part-1-2-15ac4bfd0f81)
  2018 Titouan Galopin
* [*Use env’s configuration in your application Symfony 4.1*](https://medium.com/@ronan.sau/use-envs-configuration-in-your-application-symfony-4-1-e9513abfe0a3)
  2018 Ronan Sauvage
  * [*Inject your env’s configuration in your service — second pattern*](https://medium.com/@ronan.sau/inject-your-envs-configuration-in-your-service-second-pattern-947555c440f8)
    2018 Ronan Sauvage

## Constants of parameters
* [*[Symfony] Using an application constant as a service container parameter*
  ](https://www.strangebuzz.com/en/snippets/using-an-application-constant-as-a-service-container-parameter)
  2019-10 COil
* [*Do you Autowire Services in Symfony? You can Autowire Parameters Too*](https://www.tomasvotruba.cz/blog/2018/11/05/do-you-autowire-services-in-symfony-you-can-autowire-parameters-too/)
  2018 Stefano Alletti
